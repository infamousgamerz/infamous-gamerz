//Alerts
$(document).ready (function(){
    $(".flash_message").delay(4000).slideUp(200, function() {
        $(this).alert('close');
    });
});

//Popups
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({ trigger: "hover" });
});

//Navigation drop down on hover
$(function(){
    $('.dropdown').hover(function() {
            $(this).addClass('open');
        },
        function() {
            $(this).removeClass('open');
        });
});

//Delete confirmation
function confirm_delete() {
    return confirm('Delete? Are you sure?');
}

//TinyMCE
//tinymce.init({ selector:'textarea' });
//TINYMCE
tinymce.init({
        selector: "#body, #description",
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor uploadmanager"
        ]
});
// Prevent bootstrap dialog from blocking focusin
$(document).on('focusin', function(e) {
    if ($(e.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});

unloadTiny = function(){
    tinymce.remove('textarea');
}