<?php
Class Login extends CI_Model
{
    public function __construct()
    {
        $this->load->database(); //Loads $this->db
    }
    
    function login($username, $password)
    {
        $this -> db -> select('id, username, password, role');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            $user = $query->row_array();
            $verified_pass = password_verify($password, $user['password']);
            if($verified_pass == TRUE) {
                return $query->result();
            } else {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}