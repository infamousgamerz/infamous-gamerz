<?php
class News_model extends CI_Model {

    public function __construct()
    {
        $this->load->database(); //Loads $this->db
    }

    public function getbyslug_news($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            $query = $this->db->get('news');
            return $query->result_array();
        }
        $this->db->select('news.id AS n_id,
                   news.title AS n_title,
                   news.slug AS n_slug,
                   news.body AS n_body,
                   news.featured AS n_featured,
                   news.published_date AS n_published_date,
                   news_categories.id AS nc_id,
                   news_categories.title AS nc_title,
                   news_categories.slug AS nc_slug,
                   users.id AS u_id,
                   users.username AS u_username');
        $this->db->join('news_categories', 'news_categories.id = news.category_id');
        $this->db->join('users', 'users.id = news.user_id');
        $this->db->order_by("news.id", "desc");
        $query = $this->db->get_where('news', array('news.slug' => $slug));
        return $query->row_array();
    }

    public function get_news($id = FALSE)
    {
        if (!empty($id))
        {
            $query = $this->db->get_where('news', array('id' => $id));
            return $query->row_array();
        }

        $this->db->select('news.id AS n_id,
                   news.user_id AS n_user_id,
                   news.title AS n_title,
                   news.slug AS n_slug,
                   news.body AS n_body,
                   news.featured AS n_featured,
                   news.published_date AS n_published_date,
                   news_categories.id AS nc_id,
                   news_categories.title AS nc_title,
                   news_categories.slug AS nc_slug,
                   users.id AS u_id,
                   users.username AS u_username');
        $this->db->join('news_categories', 'news_categories.id = news.category_id');
        $this->db->join('users', 'users.id = news.user_id');
        $this->db->order_by("news.id", "desc");
        $news = $this->db->get('news');
        return $news->result_array();
    }

    public function getbyslug_news_categories($slug = FALSE)
    {
        if ($slug === FALSE)
        {
            $query = $this->db->get('news_categories');
            return $query->result_array();
        }

        $query = $this->db->get_where('news_categories', array('slug' => $slug));
        return $query->row_array();
    }

    public function getbyid_news_categories($id = FALSE)
    {
        if ($id === FALSE)
        {
            $query = $this->db->get('news_categories');
            return $query->result_array();
        }

        $query = $this->db->get_where('news_categories', array('id' => $id));
        return $query->row_array();
    }

    public function getbycat_news($id = FALSE)
    {
        if ($id === FALSE)
        {
            $query = $this->db->get('news');
            return $query->result_array();
        }

        $this->db->select('news.title AS n_title,
                   news.slug AS n_slug,
                   news.body AS n_body,
                   news.published_date AS n_published_date,
                   news_categories.title AS nc_title,
                   news_categories.slug AS nc_slug,
                   users.id AS u_id,
                   users.username AS u_username');
        $this->db->join('news_categories', 'news_categories.id = news.category_id');
        $this->db->join('users', 'users.id = news.user_id');
        $this->db->order_by("news.id", "desc");
        $query = $this->db->get_where('news', array('category_id' => $id));
        return $query->result_array();
    }



    public function set_news()
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);
        $news_date = date('Y-m-d H:i:s');
        $session_data = $this->session->userdata('logged_in');
        $user_id = $session_data['id'];

        $data = array(
            'title' => $this->input->post('title'),
            'category_id' => $this->input->post('category'),
            'user_id' => $user_id,
            'slug' => $slug,
            'body' => $this->input->post('body'),
            'featured' => $this->input->post('featured'),
            'published_date' => $news_date,
            'updated_date' => $news_date
        );

        return $this->db->insert('news', $data);
    }

    public function update_news($id=0)
    {
        $this->load->helper('url');
        $this->load->library('session');
        $slug = url_title($this->input->post('title'),'dash',TRUE);
        $news_date = date('Y-m-d H:i:s');
        $session_data = $this->session->userdata('logged_in');
        $user_id = $session_data['id'];

        $data = array(
            'title' => $this->input->post('title'),
            'user_id' => $user_id,
            'slug' => $slug,
            'body' => $this->input->post('body'),
            'featured' => $this->input->post('featured'),
            'updated_date' => $news_date
        );
        $this->db->where('id',$id);
        return $this->db->update('news',$data);
    }

    public function delete_news($id) {
        $this->db->delete('news', array('id' => $id));
    }

    public function set_news_category()
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'), 'dash', TRUE);

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'body' => $this->input->post('body')
        );

        return $this->db->insert('news_categories', $data);
    }

    public function update_news_category($id=0)
    {
        $this->load->helper('url');

        $slug = url_title($this->input->post('title'),'dash',TRUE);

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $slug,
            'body' => $this->input->post('body')
        );
        $this->db->where('id',$id);
        return $this->db->update('news_categories',$data);
    }

    public function delete_news_category($id) {
        $this->db->delete('news_categories', array('id' => $id));
    }
}