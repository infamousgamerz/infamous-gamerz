<?php
class Search_model extends CI_Model {

    public function __construct()
    {
        $this->load->database(); //Loads $this->db
        $this->load->helper('form');
    }

    public function getbykeyword_search($keyword = FALSE, $category = FALSE)
    {
        if($category === 'downloads') {
            $this->db->select('downloads.title AS d_title,
                   downloads.slug AS d_slug,
                   downloads_categories.title AS dc_title,
                   downloads_categories.slug AS dc_slug');
            $this->db->join('downloads_categories', 'downloads_categories.id = downloads.category_id');
            $this->db->like('downloads.title', $keyword);
            $this->db->like('downloads.body', $keyword);
            $this->db->order_by("downloads.id", "desc");
        } elseif($category === 'news') {
            $this->db->select('news.title AS n_title,
                   news.slug AS n_slug,
                   news.body AS n_body,
                   news.published_date AS n_published_date,
                   news_categories.title AS nc_title,
                   news_categories.slug AS nc_slug');
            $this->db->join('news_categories', 'news_categories.id = news.category_id');
            $this->db->like('news.title', $keyword);
            $this->db->like('news.body', $keyword);
            $this->db->order_by("news.id", "desc");
        }
        $query = $this->db->get($category);
        return $query->result_array();
    }
}