<?php
class Users_model extends CI_Model {

    public function __construct()
    {
        $this->load->database(); //Loads $this->db
    }

    public function get_users($id = FALSE)
    {
        if ($id === FALSE)
        {
            $query = $this->db->get('users');
            return $query->result_array();
        }

        $query = $this->db->get_where('users', array('id' => $id));
        return $query->row_array();
    }

    public function set_users()
    {
        $this->load->helper('url');

        $hashed_pass = password_hash($this->input->post('password', true), PASSWORD_BCRYPT);

        $data = array(
            'username' => $this->input->post('username'),
            'role' => $this->input->post('role'),
            'password' => $hashed_pass
        );

        return $this->db->insert('users', $data);
    }

    public function update_users($id=0)
    {
        $this->load->helper('url');
        
        $hashed_pass = password_hash($this->input->post('password', true), PASSWORD_BCRYPT);

        $data = array(
            'username' => $this->input->post('username'),
            'role' => $this->input->post('role'),
            'password' => $hashed_pass
        );
        $this->db->where('id',$id);
        return $this->db->update('users',$data);
    }

    public function delete_users($id) {
        $this->db->delete('users', array('id' => $id));
    }
}