<?php
if(!empty($news_item['n_image'])) {
    echo '<img src="'.IMAGE_URL.'/'.$news_item['n_image'].'">';
}
echo '<h1 class="page-header">'.$news_item['n_title'].'</h1>';
echo '<div class="small">';
$created_date = $news_item['n_published_date'];
$posted_date = date("F jS, Y", strtotime($created_date));
echo '<span class="glyphicon glyphicon-calendar"></span>&nbsp;'.$posted_date;
echo '&nbsp;&nbsp;';
echo '<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
echo '<a href="'.BASE_URL.'/news/'.$news_item['nc_slug'].'/">'.$news_item['nc_title'].'</a>';
echo '&nbsp;&nbsp;';
echo '<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;';
echo ''.$news_item['u_username'].'';
echo '</div>';
echo $news_item['n_body'];
?>
<div id="disqus_thread"></div>
<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */
    /*
     var disqus_config = function () {
     this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
     };
     */
    (function() {  // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//infamousgamerz.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>