<?php include('nav_array.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo IMAGE_URL;?>/core/favicon.ico">

    <title><?php echo SITE_TITLE; ?> - Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL; ?>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo BASE_URL; ?>/resources/css/admin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php
//Flash message
$flash_message = $this->session->flashdata('fmessage');
if(!empty($flash_message)) {
    echo '<div class="flash_message alert alert-'.$flash_message[1].'"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$flash_message[0].'</div>';
}
?>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo BASE_URL; ?>/admin"><?php echo SITE_TITLE; ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php
                        foreach($pages_array as $page) { ?>
                            <li<?php echoActiveClassIfRequestMatches($page[2]); ?>><a href="<?php echo $page[1]; ?>"><?php echo $page[3];?> <?php echo $page[0]; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li><a href="<?php echo BASE_URL; ?>" target="_blank">Visit Site</a></li>
                <li><a href="<?php echo BASE_URL; ?>/logout">Logout</a></li>
            </ul>
            <form class="navbar-form navbar-right">
                <input type="text" class="form-control" placeholder="Search...">
            </form>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <?php
                foreach($pages_array as $page) { ?>
                    <li<?php echoActiveClassIfRequestMatches($page[2]); ?>><a href="<?php echo $page[1]; ?>"><?php echo $page[3];?> <?php echo $page[0]; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header gc-pageheader"><?php echo $title; ?></h1>