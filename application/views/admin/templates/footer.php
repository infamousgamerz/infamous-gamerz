<em>&copy; <?php echo date('Y'); ?> <?php echo SITE_TITLE; ?> - All Rights Reserved</em>
</div>
</div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo BASE_URL; ?>/resources/js/jquery.js"></script>
<script src="<?php echo BASE_URL; ?>/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>/resources/tinymce/tinymce.min.js"></script>
<script src="<?php echo BASE_URL; ?>/resources/js/default.js"></script>
</body>
</html>