<?php
function echoActiveClassIfRequestMatches($requestUri=array())
{
    $routing_uri = $_SERVER['REQUEST_URI'];
    foreach($requestUri as $ruri) {
        if (strpos($routing_uri, $ruri) !== FALSE)
            echo ' class="active"';
    }
}
$pages_array = array
(
    array(
        "News",
        "".BASE_URL."/admin/news",
        array('admin/news', 'admin/newsadd', 'admin/newsedit'),
        '<span class="glyphicon glyphicon-fire" aria-hidden="true"></span>'
    ),
    array(
        "News Categories",
        "".BASE_URL."/admin/categories",
        array('admin/categories', 'admin/categoriesadd', 'admin/categoriesedit'),
        '<span class="glyphicon glyphicon-fire" aria-hidden="true"></span>'
    ),
    array(
        "Downloads",
        "".BASE_URL."/admin/downloads",
        array('admin/downloads', 'admin/downloadsadd', 'admin/downloadsedit'),
        '<span class="glyphicon glyphicon-file" aria-hidden="true"></span>'
    ),
    array(
        "Downloads Categories",
        "".BASE_URL."/admin/download_categories",
        array('admin/download_categories', 'admin/download_categoriesadd', 'admin/download_categoriesedit'),
        '<span class="glyphicon glyphicon-file" aria-hidden="true"></span>'
    ),
    array(
        "Filemanager",
        "".BASE_URL."/admin/filemanager",
        array('admin/filemanager', 'admin/filemanager_add'),
        '<span class="glyphicon glyphicon-file" aria-hidden="true"></span>'
    ),
    array(
        "Users",
        "".BASE_URL."/admin/users",
        array('admin/users', 'admin/useradd', 'admin/useredit'),
        '<span class="glyphicon glyphicon-user" aria-hidden="true"></span>'
    )
);