<a class="pull-right btn btn-success" href="<?php echo BASE_URL;?>/admin/news/create">Add News</a>
<table class="table table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>TITLE</th>
        <th>CATEGORY</th>
        <th>ACTIONS</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($news as $news_item) {
            $session_data = $this->session->userdata('logged_in');
            if ($news_item['n_user_id'] == $session_data['id'] OR $session_data['role'] == 1) {
                echo '<tr>';
                echo '<td>' . $news_item['n_id'] . '</td>';
                echo '<td>' . $news_item['n_title'] . '</td>';
                echo '<td><a href="' . BASE_URL . '/admin/categories/update/' . $news_item['nc_id'] . '">' . $news_item['nc_slug'] . '</a></td>';
                echo '<td>';
                echo '<a class="btn btn-warning" href="' . BASE_URL . '/admin/news/update/' . $news_item['n_id'] . '">Edit</a> ';
                echo '<a class="btn btn-danger" onclick="return confirm_delete()" href="' . BASE_URL . '/admin/news/delete/' . $news_item['n_id'] . '">Delete</a>';
                echo '</td>';
                echo '</tr>';
            }
        }
        ?>
    </tbody>
</table>