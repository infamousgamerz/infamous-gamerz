<?php echo validation_errors(); ?>
<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo BASE_URL; ?>/admin/news/update/<?php echo $news_item['id']; ?>">
<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php echo $news_item['title'];?>" required>
    </div>
</div>

<div class="form-group">
    <label for="body" class="col-sm-2 control-label">Body</label>
    <div class="col-sm-10">
        <textarea id="body" name="body" class="form-control" rows="3" placeholder="Body"><?php echo $news_item['body'];?></textarea>
    </div>
</div>

    <div class="form-group">
        <label for="category" class="col-sm-2 control-label">Category</label>
        <div class="col-sm-10">
            <select class="form-control" id="category" name="category">
                <?php
                foreach($news_categories as $news_category) {
                    if($news_item['category_id'] == $news_category['id']) {$selected = 'selected="selected"';} else {$selected = '';};
                    echo '<option '.$selected.' value="'.$news_category['id'].'">'.$news_category['title'].'</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="featured" class="col-sm-2 control-label">Featured</label>
        <div class="col-sm-10">
            <select class="form-control" id="featured" name="featured">
                <option <?php if($news_item['featured'] == 0) { echo 'selected="selected"'; } ?> value="0">No</option>
                <option <?php if($news_item['featured'] == 1) { echo 'selected="selected"'; } ?>value="1">Yes</option>
            </select>
        </div>
    </div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
    </div>
</div>
</form>