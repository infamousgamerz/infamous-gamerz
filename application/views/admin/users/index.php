<a class="pull-right btn btn-success" href="<?php echo BASE_URL;?>/admin/users/create">Add User</a>
<table class="table table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>USERNAME</th>
        <th>ACTIONS</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($users as $users_item) {
            echo '<tr>';
            echo '<td>'.$users_item['id'].'</td>';
            echo '<td>'.$users_item['username'].'</td>';
            echo '<td>';
            echo '<a class="btn btn-warning" href="'.BASE_URL.'/admin/users/update/'.$users_item['id'].'">Edit</a> ';
            echo '<a class="btn btn-danger" onclick="return confirm_delete()" href="'.BASE_URL.'/admin/users/delete/'.$users_item['id'].'">Delete</a>';
            echo '</td>';
            echo '</tr>';
        }
        ?>
    </tbody>
</table>