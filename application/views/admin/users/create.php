<?php echo validation_errors(); ?>

<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo BASE_URL; ?>/admin/users/create">

<div class="form-group">
    <label for="username" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
    </div>
</div>

<div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
    </div>
</div>

    <div class="form-group">
        <label for="role" class="col-sm-2 control-label">Role</label>
        <div class="col-sm-10">
            <select class="form-control" id="role" name="role">
                <option value="0">User</option>
                <option value="1">Admin</option>
                <option value="2">Moderator</option>
            </select>
        </div>
    </div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
    </div>
</div>

</form>