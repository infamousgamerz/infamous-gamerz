<?php echo validation_errors(); ?>
<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo BASE_URL; ?>/admin/users/update/<?php echo $users_item['id']; ?>">
<div class="form-group">
    <label for="username" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo $users_item['username'];?>" required>
    </div>
</div>

<div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $users_item['password'];?>" required>
    </div>
</div>

    <div class="form-group">
        <label for="role" class="col-sm-2 control-label">Role</label>
        <div class="col-sm-10">
            <select class="form-control" id="role" name="role">
                <option <?php if($users_item['role'] == 0) { echo 'selected="selected"'; } ?> value="0">User</option>
                <option <?php if($users_item['role'] == 1) { echo 'selected="selected"'; } ?> value="1">Admin</option>
                <option <?php if($users_item['role'] == 2) { echo 'selected="selected"'; } ?> value="2">Moderator</option>
            </select>
        </div>
    </div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
    </div>
</div>
</form>