<?php
if(!empty($_GET['folder'])) { $folder = $_GET['folder']; } else { $folder = ''; };
echo validation_errors();
?>

<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo BASE_URL; ?>/admin/filemanager/folder">

    <div class="form-group">
        <label for="folder" class="col-sm-2 control-label">Folder</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="folder" name="folder" placeholder="Folder" value="<?php echo $folder; ?>">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
        </div>
    </div>

</form>