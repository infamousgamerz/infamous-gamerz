<a class="pull-right btn btn-success" href="<?php echo BASE_URL;?>/admin/categories/create">Add Category</a>
<table class="table table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>TITLE</th>
        <th>SLUG</th>
        <th>ACTIONS</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($categories as $category_item) {
            echo '<tr>';
            echo '<td>'.$category_item['id'].'</td>';
            echo '<td>'.$category_item['title'].'</td>';
            echo '<td>'.$category_item['slug'].'</td>';
            echo '<td>';
            echo '<a class="btn btn-warning" href="'.BASE_URL.'/admin/categories/update/'.$category_item['id'].'">Edit</a> ';
            echo '<a class="btn btn-danger" onclick="return confirm_delete()" href="'.BASE_URL.'/admin/categories/delete/'.$category_item['id'].'">Delete</a>';
            echo '</td>';
            echo '</tr>';
        }
        ?>
    </tbody>
</table>