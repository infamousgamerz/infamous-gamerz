<?php echo validation_errors(); ?>
<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo BASE_URL; ?>/admin/categories/update/<?php echo $category_item['id']; ?>">
<div class="form-group">
    <label for="title" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php echo $category_item['title'];?>" required>
    </div>
</div>

<div class="form-group">
    <label for="body" class="col-sm-2 control-label">Body</label>
    <div class="col-sm-10">
        <textarea id="body" name="body" class="form-control" rows="3" placeholder="Body"><?php echo $category_item['body'];?></textarea>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
    </div>
</div>
</form>