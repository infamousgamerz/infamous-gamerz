<a class="pull-right btn btn-success" href="<?php echo BASE_URL;?>/admin/downloads/create">Add Download</a>
<table class="table table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>TITLE</th>
        <th>SLUG</th>
        <th>ACTIONS</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach($downloads as $downloads_item) {
            $session_data = $this->session->userdata('logged_in');
            if($downloads_item['user_id'] == $session_data['id'] OR $session_data['role'] == 1) {
                echo '<tr>';
                echo '<td>' . $downloads_item['id'] . '</td>';
                echo '<td>' . $downloads_item['title'] . '</td>';
                echo '<td>' . $downloads_item['slug'] . '</td>';
                echo '<td>';
                echo '<a class="btn btn-warning" href="' . BASE_URL . '/admin/downloads/update/' . $downloads_item['id'] . '">Edit</a> ';
                echo '<a class="btn btn-danger" onclick="return confirm_delete()" href="' . BASE_URL . '/admin/downloads/delete/' . $downloads_item['id'] . '">Delete</a>';
                echo '</td>';
                echo '</tr>';
            }
        }
        ?>
    </tbody>
</table>