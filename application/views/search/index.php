<h1 class="page-header">Search</h1>
<form class="form-horizontal" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?php echo BASE_URL; ?>/search">

    <div class="form-group">
        <label for="keyword" class="col-sm-2 control-label">Keyword</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Keyword" value="<?php if(!empty($keyword)) { echo $keyword; }?>" required>
        </div>
    </div>

    <div class="form-group">
        <label for="category" class="col-sm-2 control-label">Category</label>
        <div class="col-sm-10">
            <select class="form-control" id="category" name="category">
                <option value="downloads" <?php if ($category == 'downloads') echo 'selected'; ?>>Downloads</option>
                <option value="news" <?php if ($category == 'news') echo 'selected'; ?>>News</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" id="submit" name="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>

<?php
if(!empty($search_results)) {
    echo '<h2>Search Results: '.$search_category.'</h2>';
    echo '<ul>';
    if($category == 'downloads') {
        foreach($search_results as $search_result) {
            echo '<li><a href="'.BASE_URL.'/downloads/'.$search_result['dc_slug'].'/'.$search_result['d_slug'].'">'.$search_result['d_title'].'</a></li>';
        }
    } elseif($category == 'news') {
        foreach($search_results as $search_result) {
            echo '<li><a href="'.BASE_URL.'/news/'.$search_result['nc_slug'].'/'.$search_result['n_slug'].'">'.$search_result['n_title'].'</a></li>';
        }
    }
    echo '</ul>';
}