<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="<?php if(!empty($metadescription)) { echo $metadescription; }  else { echo METADESCRIPTION; } ?>">
    <meta name="keywords" content="<?php echo METAKEYWORDS;?>">
    <meta name="author" content="InfamousNugz">
    <link rel="icon" href="<?php echo IMAGE_URL;?>/core/favicon.ico">

    <title>
        <?php
        echo SITE_TITLE;
        if ($title === 'Home') {
            echo ' - '.METADESCRIPTION;
        } elseif (!empty($title)) {
            echo ' - '.$title;
        } else {
            echo ' - '.METADESCRIPTION;
        }?>
    </title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_URL; ?>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="<?php echo BASE_URL; ?>/resources/css/styles.css" rel="stylesheet">
    <?php
    $bg_files = array_slice(scandir('resources/img/core'), 2);
    $i = rand(0, count($bg_files)-1);
    $selectedBg = "$bg_files[$i]";
    ?>
    <style type="text/css">
        body {
            background: url("<?php echo BASE_URL; ?>/resources/img/core/<?php echo $selectedBg; ?>") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
</head>
<body>
<?php
//Flash message
$flash_message = $this->session->flashdata('fmessage');
if(!empty($flash_message)) {
    echo '<div class="flash_message alert alert-'.$flash_message[1].'"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$flash_message[0].'</div>';
}
?>
<nav class="navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar-1" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo BASE_URL;?>">Home</a></li>
                <li><a href="<?php echo BASE_URL;?>/news">News</a></li>
                <li class="dropdown">
                    <a href="<?php echo BASE_URL;?>/downloads" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Downloads <span class="caret"></span></a>
                    <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                        <?php nav_helper(); ?>
                    </ul>
                </li>
                <li><a href="https://discord.gg/0eMY7WGblG9Ntnsi" target="_blank">Chat</a></li>
                <li><a href="<?php echo BASE_URL;?>/about">About</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo BASE_URL;?>/search">Search</a></li>
                <li><a href="<?php echo BASE_URL;?>/contact">Contact</a></li>
                <li>
                    <!-- AddToAny BEGIN -->
                    <a class="a2a_dd" href="http://www.addtoany.com/share_save">Share</a>
                    <script type="text/javascript">
                        var a2a_config = a2a_config || {};
                        a2a_config.onclick = 1;
                    </script>
                    <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

<div class="container ig-header">
<header>
    <div class="row">
        <div class="col-md-5"><h1><a href="<?php echo BASE_URL; ?>"><?php echo SITE_TITLE; ?></a></h1></div>
        <div class="col-md-7"><h2 class="pull-right"><?php echo SITE_DESCRIPTION; ?></h2></div>
    </div>
</header>
</div>

<div class="container ig-container">
    <?php include('slider.php'); ?>
    <div class="container-pages">