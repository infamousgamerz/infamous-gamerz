</div>
</div>
<div class="container ig-footer">
 <div class="ig-copyright small">&copy; Copyright <?php echo date("Y"); ?> <?php echo SITE_TITLE; ?> - <?php echo SITE_DESCRIPTION; ?></div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo BASE_URL; ?>/resources/js/jquery.js"></script>
<script src="<?php echo BASE_URL; ?>/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>/resources/js/default.js"></script>
</body>
</html>