<h1 class="page-header"><?php echo $title; ?></h1>
<p>Infamous Gamerz is a group of elite gamers formed at the turn of the century.  Originally created on Phantasy Star Online Version 2 for Dreamcast.  Soon we grew beyond it and spread to other games.</p>
<p>Currently we spend most our time in the <a href="https://discord.gg/0eMY7WGblG9Ntnsi" target="_blank">IG Chatroom</a>.  We play various games but nothing specific at this time.</p>
<p>Joining IG is easy.  Just join the <a href="https://discord.gg/0eMY7WGblG9Ntnsi" target="_blank">IG Chatroom</a> or talk to a member directly to make sure your name is not currently in use or has been in use in the past (lots of members come back).  Once then put IG or Infamous in your name and you are in!</p>
<div class="row">
	<div class="col-md-6"><iframe width="480" height="360" src="http://s29.photobucket.com/user/gwhitcher85/embed/slideshow/IG/PSO"></iframe></div>
	<div class="col-md-6"><iframe width="480" height="360" src="http://s29.photobucket.com/user/gwhitcher85/embed/slideshow/IG/PSU"></iframe></div>
</div>
<div class="row">
	<div class="col-md-6"><iframe width="480" height="360" src="http://s29.photobucket.com/user/gwhitcher85/embed/slideshow/IG/FFXIV"></iframe></div>
	<div class="col-md-6"><iframe width="480" height="360" src="https://www.youtube.com/embed/xuSte_n6yQE" frameborder="0" allowfullscreen></iframe></div>
</div>
<div class="row">
	<div class="col-md-6"><iframe width="480" height="360" src="https://www.youtube.com/embed/mrEt7H5x7Ss" frameborder="0" allowfullscreen></iframe></div>
	<div class="col-md-6"><iframe width="480" height="360" src="https://www.youtube.com/embed/xwjtWtSZZfs" frameborder="0" allowfullscreen></iframe></div>
</div>