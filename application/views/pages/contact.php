<h1 class="page-header">Contact</h1>
<?php
echo '<div class="container">
                <div class="row">
                <div class="col-md-6 col-md-offset-3 well">';
$attributes = array("class" => "form-horizontal", "name" => "contactform");
echo form_open("contact", $attributes);
echo '<fieldset>
                <legend>Contact Form</legend>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="name" class="control-label">Name</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" name="name" placeholder="Your Full Name" type="text" value="' . set_value('name') . '" />
                        <span class="text-danger">' . form_error('name') . '</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="email" class="control-label">Email ID</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" name="email" placeholder="Your Email ID" type="text" value="' . set_value('email') . '" />
                        <span class="text-danger">' . form_error('email') . '</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="subject" class="control-label">Subject</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" name="subject" placeholder="Your Subject" type="text" value="' . set_value('subject') . '" />
                        <span class="text-danger">' . form_error('subject') . '</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <label for="message" class="control-label">Message</label>
                    </div>
                    <div class="col-md-12">
                        <textarea class="form-control" name="message" rows="4" placeholder="Your Message">' . set_value('message') . '</textarea>
                        <span class="text-danger">' . form_error('message') . '</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <input name="submit" type="submit" class="btn btn-primary" value="Send" />
                    </div>
                </div>
            </fieldset>
            ' . form_close() . '
        </div>
    </div>
</div>';