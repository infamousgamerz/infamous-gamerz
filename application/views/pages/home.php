<div class="row">
    <div class="col-md-9">
    <?php
    foreach($homenews as $homenews_item) {
        echo '<h2><a href="'.BASE_URL.'/news/'.$homenews_item['nc_slug'].'/'.$homenews_item['n_slug'].'">'.$homenews_item['n_title'].'</a></h2>';
        echo '<div class="small">';
        $created_date = $homenews_item['n_published_date'];
        $posted_date = date("F jS, Y", strtotime($created_date));
        echo '<span class="glyphicon glyphicon-calendar"></span>&nbsp;'.$posted_date;
        echo '&nbsp;&nbsp;';
        echo '<span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
        echo '<a href="'.BASE_URL.'/news/'.$homenews_item['nc_slug'].'/">'.$homenews_item['nc_title'].'</a>';
        echo '&nbsp;&nbsp;';
        echo '<span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;';
        echo ''.$homenews_item['u_username'].'';
        echo '</div>';
        echo read_more($homenews_item['n_body']);
    }
    ?>
    </div>
    <div class="col-md-3">
        <h3>Top Downloads</h3>
        <ul>
        <?php
        foreach($topdownloads as $topdownloads_item) {
            echo '<li><a href="'.BASE_URL.'/downloads/'.$topdownloads_item['dc_slug'].'/'.$topdownloads_item['d_slug'].'">'.$topdownloads_item['d_title'].'</a></li>';
        }
        ?>
        </ul>
        <h3>Download Categories</h3>
        <ul>
        <?php category_helper(); ?>
        </ul>
        <h3>News Categories</h3>
        <ul>
            <?php
            foreach($newscategories as $newscategories_item) {
                echo '<li><a href="'.BASE_URL.'/news/'.$newscategories_item['slug'].'">'.$newscategories_item['title'].'</a></li>';
            }
            ?>
        </ul>
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Finfamousgamerz&tabs=timeline&width=260&height=135&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=446821865354169" width="260" height="135" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </div>
</div>