<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//News
$route['news/(:any)/(:any)'] = 'news/view/$1/$2';
$route['news/(:any)'] = 'news/category/$1';
$route['news'] = 'news';

//Search
$route['search/(:any)'] = 'search/view/$1';
$route['search'] = 'search';

//Contact Form
$route['contact'] = 'pages/contact';

//Downloads
$route['downloads/(:any)/(:any)'] = 'downloads/download_view/$1/$2';
$route['downloads/(:any)'] = 'downloads/category_view/$1';
$route['downloads'] = 'downloads';
$route['download/count/(:any)'] = 'downloads/downloads_count/$1';

//Login
$route['login'] = 'login';
$route['logout'] = 'admin/logout';
$route['VerifyLogin'] = 'VerifyLogin';

//Admin
$route['admin/news/create'] = 'admin/news_create';
$route['admin/news/update/(:any)'] = 'admin/news_update/$1';
$route['admin/news/delete/(:any)'] = 'admin/news_delete/$1';
$route['admin/news'] = 'admin/news/index';

$route['admin/categories/create'] = 'admin/categories_create';
$route['admin/categories/update/(:any)'] = 'admin/categories_update/$1';
$route['admin/categories/delete/(:any)'] = 'admin/categories_delete/$1';
$route['admin/categories'] = 'admin/categories/index';

$route['admin/downloads/create'] = 'admin/downloads_create';
$route['admin/downloads/update/(:any)'] = 'admin/downloads_update/$1';
$route['admin/downloads/delete/(:any)'] = 'admin/downloads_delete/$1';
$route['admin/downloads'] = 'admin/downloads/index';

$route['admin/download_categories/create'] = 'admin/download_categories_create';
$route['admin/download_categories/update/(:any)'] = 'admin/download_categories_update/$1';
$route['admin/download_categories/delete/(:any)'] = 'admin/download_categories_delete/$1';
$route['admin/download_categories'] = 'admin/download_categories/index';

$route['admin/filemanager/delete'] = 'admin/filemanager_delete';
$route['admin/filemanager/add'] = 'admin/filemanager_add';
$route['admin/filemanager/folder'] = 'admin/filemanager_create_folder/$1';
$route['admin/filemanager'] = 'admin/filemanager/index';

$route['admin/users/create'] = 'admin/users_create';
$route['admin/users/update/(:any)'] = 'admin/users_update/$1';
$route['admin/users/delete/(:any)'] = 'admin/users_delete/$1';
$route['admin/users'] = 'admin/users/index';

$route['admin/(:any)'] = 'admin/view/$1';
$route['admin'] = 'admin';

//Pages
$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
