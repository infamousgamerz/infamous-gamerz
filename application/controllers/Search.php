<?php
class Search extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('search_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = 'Search';
        $data['category'] = '';

        //set validation rules
        $this->form_validation->set_rules('keyword', 'Keyword', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('search/index', $data);
            $this->load->view('templates/footer', $data);
        }
        else
        {
            $data['keyword'] = $this->input->post('keyword');
            $data['category'] = $this->input->post('category');
            $data['search_category'] = ucfirst($data['category']);
            $data['search_results'] = $this->search_model->getbykeyword_search($data['keyword'], $data['category']);
            if(empty($data['search_results'])) {
                $flash_array = array('No results.', 'warning');
                $this->session->set_flashdata('fmessage', $flash_array);
                redirect('search', 'refresh');
            }
            $this->load->view('templates/header', $data);
            $this->load->view('search/index', $data);
            $this->load->view('templates/footer', $data);
        }
    }
}