<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        //Forwards to login if not logged in to all functions in this controller
        if($this->session->userdata('logged_in') == FALSE) {
            $flash_array = array('Please login first.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('login', 'refresh');
        }
    }

    function index()
    {
        $data['title'] = 'Admin';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/home', $data);
        $this->load->view('admin/templates/footer');
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('home', 'refresh');
    }

    public function view($page = 'home')
    {
        if ( ! file_exists(APPPATH.'views/admin/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/'.$page, $data);
        $this->load->view('admin/templates/footer');
    }

    public function news()
    {
        $this->load->model('news_model');
        $data['news'] = $this->news_model->get_news();
        $data['title'] = 'News';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/news/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function news_create()
    {
        $this->load->model('news_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a news item';

        $data['news_categories'] = $this->news_model->getbyslug_news_categories();

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/news/create', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->news_model->set_news();
            $flash_array = array('News successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/news', 'refresh');
        }
    }

    public function news_update($id)
    {
        $this->load->model('news_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('body','Body','required');

        $data['news_categories'] = $this->news_model->getbyslug_news_categories();

        $data['news_item'] = $this->news_model->get_news($id);
        if(empty($data['news_item']))
        {
            show_404();
        }
        $data['title'] = 'Edit '.$data['news_item']['title'].'';

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/news/update', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $session_data = $this->session->userdata('logged_in');
            if($data['news_item']['user_id'] == $session_data['id'] OR $session_data['role'] == 1) {
                $this->news_model->update_news($id);
                $flash_array = array('News with ID:#'.$id.' successfully updated.', 'success');
                $this->session->set_flashdata('fmessage', $flash_array);
            } else {
                $flash_array = array('News could not be updated.', 'danger');
                $this->session->set_flashdata('fmessage', $flash_array);
            }
            redirect('admin/news', 'refresh');
        }
    }

    public function news_delete($id) {
        $this->load->model('news_model');
        $session_data = $this->session->userdata('logged_in');
        $news = $this->news_model->get_news($id);
        if($news['user_id'] == $session_data['id'] OR $session_data['role'] == 1) {
            $this->news_model->delete_news($id);
            $flash_array = array('News with ID:#'.$id.' successfully deleted.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
        } else {
            $flash_array = array('News could not be deleted.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
        }
        redirect('admin/news', 'refresh');
    }


    public function categories()
    {
        $this->load->model('news_model');
        $data['categories'] = $this->news_model->getbyslug_news_categories();
        $data['title'] = 'Categories';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/categories/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function categories_create()
    {
        $this->load->model('news_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a category';

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/categories/create');
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->news_model->set_news_category();
            $flash_array = array('Category successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/categories', 'refresh');
        }
    }

    public function categories_update($id)
    {
        $this->load->model('news_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('body','body','required');

        $data['category_item'] = $this->news_model->getbyid_news_categories($id);
        if(empty($data['category_item']))
        {
            show_404();
        }
        $data['title'] = 'Edit '.$data['category_item']['title'];

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/categories/update', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->news_model->update_news_category($id);
            $flash_array = array('Category with ID:#'.$id.' successfully updated.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/categories', 'refresh');
        }
    }

    public function categories_delete($id) {
        $this->load->model('news_model');
        $this->news_model->delete_news_category($id);
        redirect('admin/categories', 'refresh');
    }


    public function downloads()
    {
        $this->load->model('downloads_model');
        $data['downloads'] = $this->downloads_model->getbyid_downloads();
        $data['title'] = 'Downloads';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/downloads/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function downloads_create()
    {
        $this->load->model('downloads_model');
        $data['categories'] = $this->downloads_model->getbyslug_downloads_categories();

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Add a download';

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/downloads/create', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->downloads_model->set_downloads();
            $flash_array = array('Download successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/downloads', 'refresh');
        }
    }

    public function downloads_update($id)
    {
        $this->load->model('downloads_model');
        $data['categories'] = $this->downloads_model->getbyslug_downloads_categories();

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('body','body','required');

        $data['downloads_item'] = $this->downloads_model->getbyid_downloads($id);
        if(empty($data['downloads_item']))
        {
            show_404();
        }
        $data['title'] = 'Edit '.$data['downloads_item']['title'];

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/downloads/update', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $session_data = $this->session->userdata('logged_in');
            if($data['downloads_item']['user_id'] == $session_data['id'] OR $session_data['role'] == 1) {
                $this->downloads_model->update_downloads($id);
                $flash_array = array('Download with ID:#'.$id.' successfully updated.', 'success');
                $this->session->set_flashdata('fmessage', $flash_array);
            } else {
                $flash_array = array('Download could not be updated.', 'danger');
                $this->session->set_flashdata('fmessage', $flash_array);
            }
            redirect('admin/downloads', 'refresh');
        }
    }

    public function downloads_delete($id) {
        $this->load->model('downloads_model');
        $session_data = $this->session->userdata('logged_in');
        $downloads = $this->downloads_model->get_downloads($id);
        if($downloads['user_id'] == $session_data['id'] OR $session_data['role'] == 1) {
            $this->downloads_model->delete_download($id);
            $flash_array = array('Download with ID:#'.$id.' successfully deleted.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
        } else {
            $flash_array = array('Download could not be deleted.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
        }
        redirect('admin/downloads', 'refresh');
    }


    public function download_categories()
    {
        $this->load->model('downloads_model');
        $data['categories'] = $this->downloads_model->getbyslug_downloads_categories();
        $data['title'] = 'Downloads Categories';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/downloads_categories/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function download_categories_create()
    {
        $this->load->model('downloads_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['categories'] = $this->downloads_model->get_downloads_categories();

        $data['title'] = 'Create a downloads category';

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('body', 'Body', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/downloads_categories/create', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->downloads_model->set_downloads_categories();
            $flash_array = array('Downloads category successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/download_categories', 'refresh');
        }
    }

    public function download_categories_update($id)
    {
        $this->load->model('downloads_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('body','body','required');

        $data['categories'] = $this->downloads_model->get_downloads_categories();

        $data['category_item'] = $this->downloads_model->getbyid_downloads_categories($id);
        if(empty($data['category_item']))
        {
            show_404();
        }
        $data['title'] = 'Edit '.$data['category_item']['title'];

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/downloads_categories/update', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->downloads_model->update_downloads_categories($id);
            $flash_array = array('Download category with ID:#'.$id.' successfully updated.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/download_categories', 'refresh');
        }
    }

    public function download_categories_delete($id) {
        $this->load->model('downloads_model');
        $this->downloads_model->delete_downloads_category($id);
        redirect('admin/download_categories', 'refresh');
    }


    public function filemanager()
    {
        $this->load->model('filemanager_model');
        $data['downloads'] = $this->filemanager_model->get_files();
        $data['title'] = 'Filemanager';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/filemanager/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function filemanager_add()
    {
        $this->load->model('filemanager_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Add a file';

        $this->form_validation->set_rules('folder', 'folder', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/filemanager/add');
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $folder = $this->input->post('folder');
            $folder = str_replace('../','',$folder);
			$folder .= (substr($folder, -1) == '/' ? '' : '/');
            $this->filemanager_model->do_upload($folder);
            $flash_array = array('File successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/filemanager', 'refresh');
        }
    }

    public function filemanager_create_folder() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('folder', 'folder', 'required');

        $data['title'] = 'Create a folder';
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/filemanager/folder');
            $this->load->view('admin/templates/footer');

        }
        else {
            $folder = $this->input->post('folder');
            $folder = str_replace('../','',$folder);
			$folder .= (substr($folder, -1) == '/' ? '' : '/');
			$folder .= (substr($folder, 0, 1) == '/' ? '' : '/');
            mkdir('./resources/downloads/'.$folder, 0777);
            $flash_array = array('Folder: '.$folder.' successfully created.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/filemanager', 'refresh');
        }
    }

    public function filemanager_delete() {
        $folder = $_GET['file'];
        $folder = str_replace('../','',$folder);
        unlink(getcwd().'/resources/downloads'.$folder);
        $flash_array = array('File successfully created.', 'success');
        $this->session->set_flashdata('fmessage', $flash_array);
        redirect('admin/filemanager', 'refresh');
    }


    public function users()
    {
        $this->load->model('users_model');
        $data['users'] = $this->users_model->get_users();
        $data['title'] = 'Users';

        $loggedin = $this->session->userdata('logged_in');
        if($loggedin['role'] != 1) {
            $flash_array = array('Only administrators can edit users.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin', 'refresh');
        }
        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/users/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function users_create()
    {
        $this->load->model('users_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a user';

        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('password','Password','required');

        $loggedin = $this->session->userdata('logged_in');
        if($loggedin['role'] != 1) {
            $flash_array = array('Only administrators can edit users.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin', 'refresh');
        }

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/users/create');
            $this->load->view('admin/templates/footer');
        }
        else
        {
            $this->users_model->set_users();
            $flash_array = array('User successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/users', 'refresh');
        }
    }

    public function users_update($id)
    {
        $this->load->model('users_model');

        $data['title'] = 'Edit a user';

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('password','Password','required');

        $data['users_item'] = $this->users_model->get_users($id);
        if(empty($data['users_item']))
        {
            show_404();
        }

        $loggedin = $this->session->userdata('logged_in');
        if($loggedin['role'] != 1) {
            $flash_array = array('Only administrators can edit users.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin', 'refresh');
        }

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/users/update', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->users_model->update_users($id);
            $flash_array = array('User with ID:#'.$id.' successfully updated.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/users', 'refresh');
        }
    }

    public function users_delete($id) {
        $loggedin = $this->session->userdata('logged_in');
        if($loggedin['role'] != 1) {
            $flash_array = array('Only administrators can edit users.', 'danger');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin', 'refresh');
        }
        $this->load->model('users_model');
        $this->users_model->delete_users($id);
        redirect('admin/users', 'refresh');
    }

    public function products()
    {
        $this->load->model('products_model');
        $data['products'] = $this->products_model->get_products();
        $data['title'] = 'Products';

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/products/index', $data);
        $this->load->view('admin/templates/footer');
    }

    public function products_create()
    {
        $this->load->model('products_model');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create a product';

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('text', 'Text', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/products/create', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->products_model->set_products();
            $flash_array = array('Product successfully added.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/products', 'refresh');
        }
    }

    public function products_update($id)
    {
        $this->load->model('products_model');

        $data['title'] = 'Edit a product';

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title','Title','required');
        $this->form_validation->set_rules('text','text','required');

        $data['products_categories'] = $this->products_model->get_products_categories();
        $data['products_series'] = $this->products_model->get_products_series();

        $data['products_item'] = $this->products_model->get_products($id);
        if(empty($data['products_item']))
        {
            show_404();
        }

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/products/update', $data);
            $this->load->view('admin/templates/footer');

        }
        else
        {
            $this->products_model->update_products($id);
            $flash_array = array('Product with ID:#'.$id.' successfully updated.', 'success');
            $this->session->set_flashdata('fmessage', $flash_array);
            redirect('admin/products', 'refresh');
        }
    }

    public function products_delete($id) {
        $this->load->model('products_model');
        $this->products_model->delete_products($id);
        redirect('admin/products', 'refresh');
    }

}