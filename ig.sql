-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2016 at 04:02 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ig`
--

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE IF NOT EXISTS `downloads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(555) NOT NULL,
  `body` text NOT NULL,
  `download_image` varchar(255) NOT NULL,
  `download_url` varchar(255) NOT NULL,
  `download_mirror` text NOT NULL,
  `download_count` int(11) NOT NULL,
  `download_order` int(11) NOT NULL,
  `featured` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `user_id`, `category_id`, `title`, `slug`, `body`, `download_image`, `download_url`, `download_mirror`, `download_count`, `download_order`, `featured`) VALUES
(1, 2, 2, 'Test download', 'test-download', '<p>this is a test download</p>', 'test', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `downloads_categories`
--

CREATE TABLE IF NOT EXISTS `downloads_categories` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(535) NOT NULL,
  `slug` varchar(535) NOT NULL,
  `body` text NOT NULL,
  `keywords` varchar(535) NOT NULL,
  `image` varchar(535) NOT NULL,
  `no_subs` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads_categories`
--

INSERT INTO `downloads_categories` (`id`, `parent_id`, `title`, `slug`, `body`, `keywords`, `image`, `no_subs`) VALUES
(1, 0, 'Phantasy Star Online', 'phantasy-star-online', '<p>Phantasy Star Online downloads.</p>', '', '', 0),
(2, 1, 'PSOPC', 'psopc', '<p>Downloads for Phantasy Star Online PC version.</p>', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` text NOT NULL,
  `body` text NOT NULL,
  `featured` int(11) NOT NULL,
  `published_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `user_id`, `category_id`, `title`, `slug`, `body`, `featured`, `published_date`, `updated_date`) VALUES
(1, 1, 1, 'Welcome to the new IG site!', 'welcome-to-the-new-ig-site', '<p>Welcome to the new IG site.&nbsp; We''ve made this website many times over the years but I think this one is here to stay for awhile.&nbsp; This is a custom built website using the Codeigniter PHP framework and Bootstrap 3 CSS framework.&nbsp; This was built to make more of an archive type website rather than a blog like we have in previous years.&nbsp; I don''t really see a need for forums anymore so I will not be putting them up or building anything new.&nbsp; I will probably just delete what was there once this sites downloads become more active.&nbsp; Anyways I hope you enjoy!</p>', 0, '2016-04-11 00:00:00', '2016-04-13 20:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

CREATE TABLE IF NOT EXISTS `news_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` text NOT NULL,
  `body` text NOT NULL,
  `keywords` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_categories`
--

INSERT INTO `news_categories` (`id`, `title`, `slug`, `body`, `keywords`) VALUES
(1, 'Announcements', 'announcements', '<p>These are announcements about new features and updates to the website or Infamous Gamerz.</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` tinyint(4) NOT NULL,
  `role` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `username`, `password`) VALUES
(1, 1, 'admin', '$2y$10$oQHtRsUwHm8DJjT8fWJhx.H37SdruSFLUSWo2hwlC7vf6g48Nc3Ue');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads_categories`
--
ALTER TABLE `downloads_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_categories`
--
ALTER TABLE `news_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `downloads_categories`
--
ALTER TABLE `downloads_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news_categories`
--
ALTER TABLE `news_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
